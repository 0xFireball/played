#!/usr/bin/env python3
# Copyright (c) 2017-2018 Sampo Sorsa <sorsasampo@protonmail.com>

import argparse
from datetime import datetime, timedelta
import logging
import os
import os.path
import shlex
import shutil
import sqlite3
import subprocess
import threading
from xdg import BaseDirectory

DATA_DIR = BaseDirectory.save_data_path('played')
SQLITE_DB = os.path.join(DATA_DIR, 'played.sqlite')

TS_FORMAT = '%Y-%m-%d %H:%M'


class Database:
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        self.conn.row_factory = sqlite3.Row
        self.c = self.conn.cursor()
        self.c.execute('PRAGMA foreign_keys = ON')

    def add_entry(self, exe_id, ts_start, ts_end, idle, exitcode):
        self.c.execute('INSERT INTO entry(exe_id, ts_start, ts_end, idle, exitcode) VALUES(?, ?, ?, ?, ?)', (exe_id, ts_start, ts_end, idle, exitcode))
        self.conn.commit()

    def find_last_exe_by_game_name(self, game_name):
        self.c.execute("""
                       SELECT exe.id, exe.path, exe.args
                       FROM game JOIN exe ON game.id = exe.game_id
                       JOIN entry ON entry.exe_id = exe.id
                       WHERE game.name = ?
                       ORDER BY entry.ts_start DESC
                       LIMIT 1
                       """, (game_name,))
        return self.c.fetchone()

    def get_entries(self, game_name):
        self.c.execute(
            "SELECT game.name, exe.path, exe.args, entry.ts_start, strftime('%s', entry.ts_end) - strftime('%s', entry.ts_start) AS secs, entry.exitcode"
            " FROM entry"
            " JOIN exe ON entry.exe_id = exe.id"
            " JOIN game ON exe.game_id = game.id"
            " WHERE ? IS NULL OR game.name = ?"
            " ORDER BY entry.ts_start",
            (game_name, game_name,))
        return self.c.fetchall()

    def get_game_id(self, game_name):
        self.c.execute('INSERT OR IGNORE INTO game(name) VALUES(?)', (game_name,))
        self.conn.commit()
        self.c.execute('SELECT id FROM game WHERE name = ?', (game_name,))
        return self.c.fetchone()[0]

    def get_last_games(self, limit=None):
        self.c.execute(
            "SELECT game.name, MAX(entry.ts_start) AS \"lastplayed [timestamp]\""
            " FROM entry"
            " JOIN exe ON entry.exe_id = exe.id"
            " JOIN game ON exe.game_id = game.id"
            " GROUP BY game.id"
            " ORDER BY MAX(entry.ts_start) DESC"
            " LIMIT ?",
            (limit or -1,)
        )
        return [r for r in reversed(self.c.fetchall())]

    def get_or_create_exe(self, game_id, exe, args):
        self.c.execute('INSERT OR IGNORE INTO exe(game_id, path, args) VALUES(?, ?, ?)', (game_id, exe, args))
        self.conn.commit()
        self.c.execute('SELECT id FROM exe WHERE game_id = ? AND path = ? AND args = ?', (game_id, exe, args))
        return self.c.fetchone()[0]

    def get_stats(self, game_name, within_days=None, limit=None):
        self.c.execute("""
                       SELECT game.name, COUNT(*) AS entries, SUM(strftime('%s', ts_end) - strftime('%s', ts_start)) AS secs
                       FROM game
                       JOIN exe ON game.id = exe.game_id
                       JOIN entry ON exe.id = entry.exe_id
                       WHERE (? IS NULL OR name = ?)
                       AND (? IS NULL OR DATE(entry.ts_start) >= DATE('now', '-' || ? || ' days'))
                       GROUP BY game.name
                       ORDER BY secs DESC
                       LIMIT ?
                       """, (game_name, game_name, within_days, within_days, limit or -1))
        return [{'game': r[0], 'entries': r[1], 'secs': r[2]} for r in reversed(self.c.fetchall())]

    def migrate(self):
        # Use user_version pragma to store migration version
        # https://sqlite.org/pragma.html#pragma_user_version
        self.c.execute('PRAGMA user_version')
        user_version = self.c.fetchone()[0]
        if user_version < 1:
            self.c.execute("""
                           CREATE TABLE game(
                               id INTEGER PRIMARY KEY,
                               name TEXT NOT NULL UNIQUE
                           )""")
            self.c.execute("""
                           CREATE TABLE exe(
                               id INTEGER PRIMARY KEY,
                               game_id INTEGER NOT NULL REFERENCES game(id) ON DELETE CASCADE,
                               path TEXT NOT NULL,
                               args TEXT NOT NULL,
                               UNIQUE(path, args)
                           )""")
            self.c.execute("""
                           CREATE TABLE entry(
                               id INTEGER PRIMARY KEY,
                               exe_id INTEGER NOT NULL REFERENCES exe(id) ON DELETE CASCADE,
                               ts_start TIMESTAMP NOT NULL,
                               ts_end TIMESTAMP,
                               idle INTEGER NOT NULL DEFAULT 0,
                               exitcode INTEGER
                           )""")
        if user_version < 2:
            self.c.execute('PRAGMA foreign_keys=OFF')
            self.c.execute('ALTER TABLE exe RENAME TO exe_old')
            self.c.execute("""
                           CREATE TABLE exe(
                               id INTEGER PRIMARY KEY,
                               game_id INTEGER NOT NULL REFERENCES game(id) ON DELETE CASCADE,
                               path TEXT NOT NULL,
                               args TEXT NOT NULL,
                               UNIQUE(game_id, path, args)
                           )""")
            self.c.execute('INSERT INTO exe(id, game_id, path, args) '
                           'SELECT id, game_id, path, args FROM exe_old')
            self.c.execute('DROP TABLE exe_old')
            self.c.execute('PRAGMA foreign_keys=ON')

        current_version = 2
        if user_version < current_version:
            self.c.execute('PRAGMA user_version = %d' % current_version)
            self.conn.commit()


class IdleTime(threading.Thread):
    def __init__(self, threshold=60):
        threading.Thread.__init__(self)
        self.threshold = threshold
        self.reset()
        self.stopping = threading.Event()

    def get_current_idle_time(self):
        if 'DISPLAY' not in os.environ:
            # report no idle time in unit tests
            return 0
        cp = subprocess.run(['xprintidle'], stdout=subprocess.PIPE)
        if cp.returncode != 0:
            return None
        return int(cp.stdout)

    def reset(self):
        self.idle_total = 0
        self.idle_start = datetime.now()
        self.idle_end = self.idle_start
        self.idle_last = 0

    def run(self):
        while not self.stopping.is_set():
            self.tick()
            self.stopping.wait(15)
        self.tick()
        self.stopping.clear()

        # count end as idle time
        self.idle_end = datetime.now()
        idle_entry = self.idle_end - self.idle_start
        if idle_entry.total_seconds() > self.threshold:
            self.idle_total += idle_entry.total_seconds()

    def stop(self):
        self.stopping.set()

    def tick(self):
        idle_now = self.get_current_idle_time()
        if idle_now is None:
            return

        if idle_now >= self.idle_last:
            # idle time starts/continues
            self.idle_start = datetime.now() - timedelta(milliseconds=idle_now)
            self.idle_end = datetime.now()
        else:
            # idle time reset
            idle_entry = self.idle_end - self.idle_start
            if idle_entry.total_seconds() > self.threshold:
                self.idle_total += idle_entry.total_seconds()
        self.idle_last = idle_now


def last(db, limit=None):
    games = db.get_last_games(limit)
    for game in games:
        print('%s %s' % (game['lastplayed'].strftime(TS_FORMAT), game['name']))


def parse_args():
    parser = argparse.ArgumentParser(description='Track time games have been played for.')
    subparsers = parser.add_subparsers(dest='command')

    parser_last = subparsers.add_parser('last', description='Show last games played.')
    parser_last.add_argument('-n, --limit', type=int, help='Maximum number of games to show', metavar='limit', dest='limit')

    parser_play = subparsers.add_parser('play')
    parser_play.add_argument('game', help='Game name')
    parser_play.add_argument('executable', nargs='?', help='Path to executable')
    parser_play.add_argument('args', nargs=argparse.REMAINDER, help='Arguments to executable')

    parser_sessions = subparsers.add_parser('sessions')
    parser_sessions.add_argument('game', nargs='?', help='Optional game name')

    parser_sqlite = subparsers.add_parser('sqlite')
    parser_sqlite.add_argument('args', metavar='ARG', nargs=argparse.REMAINDER, help='sqlite command')

    parser_stats = subparsers.add_parser('stats')
    parser_stats.add_argument('-n, --limit', type=int, help='Maximum number of games to show', metavar='limit', dest='limit')
    parser_stats.add_argument('-d, --days', type=int, help='Number of days to show stats for', metavar='days', dest='days')
    parser_stats.add_argument('-f, --format', type=int, help='Format for time played', metavar='fmt', dest='fmt')
    parser_stats.add_argument('game', nargs='?', help='Optional game name')

    args = parser.parse_args()
    return args


def play(db, game_name, executable, args):
    game_id = db.get_game_id(game_name)

    if executable is not None:
        if not os.path.isabs(executable):
            executable = shutil.which(executable)

        escaped_args = ' '.join([shlex.quote(i) for i in (args or [])])
        exe_id = db.get_or_create_exe(game_id, executable, escaped_args)
        track(db, exe_id, executable, args)
    else:
        exe = db.find_last_exe_by_game_name(game_name)
        if exe is not None:
            track(db, exe['id'], exe['path'], shlex.split(exe['args']))
        else:
            return False
    print('Played within:')
    played_today = db.get_stats(game_name, 1)[0]
    print('Today:   %s, %d sessions' % (secs2time(played_today['secs']), played_today['entries']))
    for days in [7, 14]:
        played = db.get_stats(game_name, days)[0]
        print('%2d days: %s, %d sessions' % (days, secs2time(played['secs']), played['entries']))
    return True


def secs2time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    days, hours = divmod(hours, 24)
    return '%3dd %2dh %2dm %2ds' % (days, hours, mins, secs)


def sessions(db, game_name):
    rows = db.get_entries(game_name)
    for r in rows:
        print('%s %-16s\t%s' % (r['ts_start'].strftime(TS_FORMAT), r['name'], secs2time(r['secs'])))


def stats(db, game=None, within_days=None, limit=None, fmt=0):
    rows = db.get_stats(game, within_days=within_days, limit=limit)
    for r in rows:
        secs = r['secs']
        timestr = ''
        if fmt == 0:
            timestr = secs2time(secs)
        elif fmt == 1:
            mins, secs = divmod(secs, 60)
            hours, mins = divmod(mins, 60)
            if hours > 0:
                timestr = f'{hours}h'
            else:
                timestr = '%02dm' % mins
        print('%-40s %4d %s' % (r['game'], r['entries'], timestr))


def track(db, exe_id, executable, args):
    ts_start = datetime.now()
    idle = IdleTime()
    idle.start()

    p = subprocess.Popen([executable] + args)
    try:
        p.wait()
    except KeyboardInterrupt:
        logging.warn('KeyboardInterrupt, terminating process with 10sec grace period...')
        try:
            p.terminate()
        except OSError:
            logging.exception('terminate() raised exception')
        try:
            p.wait(10)
        except subprocess.TimeoutExpired:
            logging.warn('Process still running after grace period, killing')
            p.kill()
            p.wait()

    ts_end = datetime.now()
    idle.stop()
    idle.join()
    idle_secs = idle.idle_total

    db.add_entry(exe_id, ts_start, ts_end, p.returncode, idle_secs)

    secs = (ts_end - ts_start).total_seconds()
    logging.info('Session: %s, idle: %s' % (secs2time(secs), secs2time(idle_secs)))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    args = parse_args()

    db = Database(SQLITE_DB)
    db.migrate()

    if args.command is None:
        stats(db)
    elif args.command == 'play':
        if not play(db, args.game, args.executable, args.args):
            logging.error('Could not find executable for "%s"' % args.game)
    elif args.command == 'sessions':
        sessions(db, args.game)
    elif args.command == 'sqlite':
        subprocess_args = ['sqlite3', SQLITE_DB] + args.args
        subprocess.run(subprocess_args)
    elif args.command == 'stats':
        stats(db, args.game, args.days, args.limit, args.fmt)
    elif args.command == 'last':
        last(db, args.limit)
