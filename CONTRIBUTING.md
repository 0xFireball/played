## Signoff

Contributions to this project must be signed off using `git commit --signoff`
to certify:

1. Agreement to [Developer's Certificate of Origin v1.1][DCO].
2. Agreement to submit the commit under [Apache License v2][ALv2] and
   [GNU General Public License v3][GPLv3], or any later version of either.

[ALv2]: https://www.apache.org/licenses/LICENSE-2.0
[DCO]: https://developercertificate.org/
[GPLv3]: https://www.gnu.org/licenses/gpl.html
